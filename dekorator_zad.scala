import scala.io.Source

/**
  * Created by zezima on 10.04.17.
  */

trait TraitReader{

  def whiteSpaceClean: String
  def upperCase: String
}

class HelpReader extends TraitReader {

  override def upperCase        = "konwertuje litery na wielkie"
  override def whiteSpaceClean  = "usuwa spacje"
}

abstract class Reader(decoratedReader : TraitReader) extends TraitReader {

  override def whiteSpaceClean  = decoratedReader.whiteSpaceClean
  override def upperCase        = decoratedReader.upperCase
}

class InputReader(decoratedReader : TraitReader) extends Reader(decoratedReader) {

  def readLineUpper () :String = {
    println("Napisz cos i zatwierdz enterem (uppercase)")
    val scanner = new java.util.Scanner(System.in)
    return scanner.nextLine()
  }

  def readLineWhite () :String = {
    println("Napisz cos i zatwierdz enterem (whitespace)")
    val scanner = new java.util.Scanner(System.in)
    return scanner.nextLine()
  }

  override def upperCase: String        = return readLineUpper().toUpperCase()
  override def whiteSpaceClean: String  = return readLineWhite().filterNot((x: Char) => x.isWhitespace)

}

class FileReader(decoratedReader : TraitReader) extends Reader(decoratedReader) {

  val fileName = "src/dekorator_zad.scala"

  def readFile() : String = return Source.fromFile(fileName).getLines.mkString

  override def upperCase: String        = return readFile().toUpperCase()
  override def whiteSpaceClean: String  = return readFile().filterNot((x: Char) => x.isWhitespace)
}

object Main {

  def printText(traitReader: TraitReader) = println("Upper: " + traitReader.upperCase + " \nBez spacji: " + traitReader.whiteSpaceClean)

  def main(args: Array[String]): Unit = {
    var read: TraitReader = new HelpReader
    printText(read)

    read = new InputReader(read)
    printText(read)

    read = new FileReader(read)
    printText(read)
  }
}
Main.main(Array())